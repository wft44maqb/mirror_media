> 《眾新聞》無黨無派，熱愛香港，希望推動公共參與，一同維繫香港自由開放、多元包容等核心價值。我們不設既定政治立場或政策傾向。只願一點燭光，點燃一片燭海，一同發光發熱。

![](https://gitlab.com/wft44maqb/mirror_media/-/raw/main/%E7%9C%BE%E6%96%B0%E8%81%9E/%E6%84%88%E9%BB%91%E6%9A%97%E3%80%80%E7%9C%9F%E7%9B%B8%E6%84%88%E9%87%8D%E8%A6%81%E3%80%80%E8%AB%8B%E6%94%AF%E6%8C%81%E7%9C%BE%E6%96%B0%E8%81%9E%E4%B8%AD%E5%9C%8B%E7%B5%84-HY07NVjcW2M.mp4)

##### 官方網站: https://www.hkcnews.com/
##### Youtube: https://www.youtube.com/channel/UC7K4DBOzdITZFOjGkea_CCA
##### Instagram: https://www.instagram.com/hkcnews/
##### Facebook: https://www.facebook.com/hkcnews/
##### Telegram: https://t.me/hkcnews

![](https://gitlab.com/wft44maqb/mirror_media/-/raw/main/%E7%9C%BE%E6%96%B0%E8%81%9E/screencapture-zh-wikipedia-org-zh-cn-2021-07-18-14_24_28.png)
