> The News Lens 關鍵評論網 是一個媒體實驗。
> 希望能夠提供多元觀點，讓一個事件有不同切入角度，讓社群世代的讀者，能輕鬆分享和參與討論。
> 
> The News Lens 關鍵評論網 設立於2013年8月。
> 我們是一群對於現今媒體不滿又想要做出一些改變的人。
> 有一直在媒體產業的編輯，有多年在海外各地的專業經理人，還有對於網路技術有熱情的開發者。
> 
> 我們夢想中的媒體是除了陳述事實之外，還能夠提供多元、不同方向的觀點，並為智慧手機、平板、電腦等不同平台提供各自適合閱讀內容。
> 也讓社交網路世代的使用者，能夠更輕鬆的分享、討論和參與他們有興趣的議題。


##### 官方網站: https://www.thenewslens.com/
##### Youtube: https://www.youtube.com/c/TheNewsLens
##### Telegram: https://t.me/thenewslens
##### 維基百科: [https://zh.wikipedia.org/wiki/關鍵評論網](https://zh.wikipedia.org/wiki/%E9%97%9C%E9%8D%B5%E8%A9%95%E8%AB%96%E7%B6%B2)
##### Facebook: https://www.facebook.com/TheNewsLens/

![](https://gitlab.com/wft44maqb/mirror_media/-/raw/main/%E9%97%9C%E9%8D%B5%E8%A9%95%E8%AB%96%E7%B6%B2/screencapture-zh-wikipedia-org-zh-cn-2021-07-18-14_29_18.png)
